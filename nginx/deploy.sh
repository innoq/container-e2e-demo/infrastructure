#!/bin/bash

helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update

helm install -f values.yml nginx nginx-stable/nginx-ingress