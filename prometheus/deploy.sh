#!/bin/bash

helm uninstall prometheus

sleep 10

helm install -f values.yaml prometheus stable/prometheus

kubectl apply -f prometheus-gatekeeper-deployment.yml
kubectl apply -f prometheus-gatekeeper-ingress.yml
kubectl apply -f prometheus-gatekeeper-service.yml

echo ""
echo "!!! PLEASE remember to apply the patch mentioned here in the Keycloak realm: https://issues.redhat.com/browse/KEYCLOAK-8954 !!!"
echo ""