#!/bin/bash

# https://itsmetommy.com/2019/06/14/kubernetes-automated-dns-with-externaldns-on-gke/

export SERIVCE_NAME=container-e2e-demo-dns
export PROJECT_ID=ses-environment

gcloud iam service-accounts create $SERIVCE_NAME \
--display-name "$SERIVCE_NAME service account for external-dns" \
--project $PROJECT_ID

gcloud iam service-accounts keys create ./credentials.json \
--iam-account $SERIVCE_NAME@$PROJECT_ID.iam.gserviceaccount.com \
--project $PROJECT_ID

gcloud projects add-iam-policy-binding $PROJECT_ID \
--member serviceAccount:$SERIVCE_NAME@$PROJECT_ID.iam.gserviceaccount.com --role roles/dns.admin

kubectl create namespace external-dns
kubectl delete secret --namespace=external-dns external-dns
kubectl create secret generic external-dns --from-file=./credentials.json --namespace=external-dns

helm repo add bitnami https://charts.bitnami.com/bitnami

helm uninstall external-dns
helm install \
  --namespace external-dns \
  --set provider=google \
  --set google.project=$PROJECT_ID \
  --set google.serviceAccountSecret=external-dns \
  --set policy=sync \
  --set registry=txt \
  --set txtOwnerId=k8s \
  --set domainFilters={ses.innoq.io} \
  --set rbac.create=true \
  external-dns \
  bitnami/external-dns