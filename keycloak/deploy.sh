#!/bin/bash

helm repo add codecentric https://codecentric.github.io/helm-charts

kubectl create secret generic realm-secret --from-file=container-e2e-demo-realm.json

helm uninstall keycloak
helm install keycloak -f values.yaml codecentric/keycloak

echo "Keycloak user: keycloak"
echo "Password: "
kubectl get secret --namespace default keycloak-http -o jsonpath="{.data.password}" | base64 --decode; echo