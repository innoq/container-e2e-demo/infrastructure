#!/bin/bash

helm uninstall grafana

helm install -f values.yaml grafana stable/grafana

echo ""
echo "!!! PLEASE remember to apply the patch mentioned here in the Keycloak realm: https://issues.redhat.com/browse/KEYCLOAK-8954 !!!"
echo ""

echo "Grafana user: admin"
echo "Password: "
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo